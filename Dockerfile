FROM python:3.11-alpine

ENV APP_DIR=/app/ \
    POETRY_VERSION=1.4.0 \
    LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONPATH=/

WORKDIR ${APP_DIR}

RUN pip install "poetry==$POETRY_VERSION"
COPY poetry.lock pyproject.toml ${APP_DIR}
RUN poetry config virtualenvs.create false \
    && poetry install --only main --no-interaction --no-ansi

COPY app ${APP_DIR}

CMD ["python", "main.py"]
