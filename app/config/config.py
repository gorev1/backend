from enum import Enum
from pathlib import Path

from pydantic import BaseSettings, PostgresDsn

CONFIG_DIR = Path(__file__).resolve().parent


class Environment(Enum):
    """Где запущен сервис"""

    LOCAL = "local"
    DEV = "dev"
    PROD = "prod"


class HttpService(BaseSettings):
    """Конфиги http сервиса"""

    host: str = "0.0.0.0"
    port: int = 7000


class Database(BaseSettings):
    dialect: str = "postgresql"
    api: str = "asyncpg"
    user: str = "postgres"
    password: str = "password"
    host: str = "localhost"
    port: str = "5432"
    db_name: str = "store"
    future: bool = True
    pool_recycle: int = 30 * 60
    db_echo: bool = True

    @property
    def url(self) -> str:
        """Url до базы данных со всеми допами"""
        return PostgresDsn.build(
            scheme=f"{self.dialect}+{self.api}",
            user=self.user,
            password=self.password,
            host=self.host,
            port=self.port,
            path=f"/{self.db_name}",
        )

    def config(self) -> dict:
        """Конфиг для sqlalchemy"""
        return {
            "url": self.url,
            "future": self.future,
            "pool_recycle": self.pool_recycle,
            "echo": self.db_echo,
        }


class Cipher(BaseSettings):
    secret: str = "d040f0a1599cbe81337201f1afd3b8476df6ad510fa589240ed18b30e47cd3a3"


class _Config(BaseSettings):
    debug: bool = False
    environment: Environment | str = Environment.LOCAL
    root_path: str = ""
    https_proxy_enabled: bool = True

    http_service = HttpService()
    database = Database()
    cipher = Cipher()

    class Config:
        env_file = str(CONFIG_DIR / ".env")
        env_nested_delimiter = "__"


Config: _Config = _Config()
