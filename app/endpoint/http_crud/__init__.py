# mypy: disable-error-code=attr-defined
from fastapi import APIRouter

from . import category, item, order, user

crud_router = APIRouter(prefix="/api")
crud_router.include_router(user.router)
crud_router.include_router(item.router)
crud_router.include_router(category.router)
crud_router.include_router(order.router)
