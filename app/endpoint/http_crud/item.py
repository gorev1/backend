# mypy: disable-error-code="arg-type, misc, valid-type"
import asyncio

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.item.crud import ItemCrud
from app.db.session import get_session
from app.schema.common import ManyResponse
from app.schema.item import ItemFilter, ItemOrder
from app.service.paginator import Paginator

router = APIRouter(prefix="/item", tags=["item"])
crud_cls = ItemCrud


@router.get(
    "",
    response_model=ManyResponse[crud_cls.schema_out],
)
async def get_many(
    paginator: Paginator = Depends(),
    order: ItemOrder = ItemOrder.NAME.asc,
    filter: ItemFilter = Depends(),
    db_session: AsyncSession = Depends(get_session),
):
    crud = crud_cls(db_session)
    items, total = await asyncio.gather(
        crud.get_many(
            paginator=paginator,
            order=order.to_order_generator(),
            filter_generator=filter,
        ),
        crud.count(filter_generator=filter),
    )
    return {
        "items": items,
        "total": total,
    }


@router.get(
    "/{id}",
    response_model=crud_cls.schema_out,
)
async def get_one(
    id: int,
    db_session: AsyncSession = Depends(get_session),
):
    return await crud_cls(db_session).get_by_id(id)


@router.post(
    "",
    response_model=crud_cls.schema_out,
)
async def create(
    data: crud_cls.schema_in,
    db_session: AsyncSession = Depends(get_session),
):
    return await crud_cls(db_session).create(data)


@router.put(
    "/{id}",
    response_model=crud_cls.schema_out,
)
async def update(
    id: int,
    data: crud_cls.schema_update,
    db_session: AsyncSession = Depends(get_session),
):
    return await crud_cls(db_session).update(crud_cls.model.id == id, data)


@router.delete(
    "/{id}",
)
async def delete(
    id: int,
    db_session: AsyncSession = Depends(get_session),
):
    return await crud_cls(db_session).delete(crud_cls.model.id == id)
