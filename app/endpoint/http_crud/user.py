# mypy: disable-error-code="arg-type, misc, valid-type"
import asyncio

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.session import get_session
from app.db.user.crud import UserCrud
from app.schema.common import ManyResponse
from app.schema.user import UserInHttp, UserOutHttp, UserUpdateHttp
from app.service.paginator import Paginator

router = APIRouter(prefix="/user", tags=["user"])
crud_cls = UserCrud


@router.get(
    "",
    response_model=ManyResponse[UserOutHttp],
)
async def get_many(
    paginator: Paginator = Depends(),
    db_session: AsyncSession = Depends(get_session),
):
    crud = crud_cls(db_session)
    items, total = await asyncio.gather(
        crud.get_many(
            paginator=paginator,
        ),
        crud.count(filter_generator=filter),
    )
    return {
        "items": items,
        "total": total,
    }


@router.get(
    "/{id}",
    response_model=UserOutHttp,
)
async def get_one(
    id: int,
    db_session: AsyncSession = Depends(get_session),
):
    return await crud_cls(db_session).get_by_id(id)


@router.post(
    "",
    response_model=UserOutHttp,
)
async def create(
    data: UserInHttp,
    db_session: AsyncSession = Depends(get_session),
):
    return await crud_cls(db_session).create(
        crud_cls.schema_in(
            **data.dict(),
            hashed_password=data.password,
        )
    )


@router.put(
    "/{id}",
    response_model=UserOutHttp,
)
async def update(
    id: int,
    data: UserUpdateHttp,
    db_session: AsyncSession = Depends(get_session),
):
    return await crud_cls(db_session).update(
        crud_cls.model.id == id,
        crud_cls.schema_update(
            **data.dict(),
            hashed_password=data.password,
        ),
    )


@router.delete(
    "/{id}",
)
async def delete(
    id: int,
    db_session: AsyncSession = Depends(get_session),
):
    return await crud_cls(db_session).delete(crud_cls.model.id == id)
