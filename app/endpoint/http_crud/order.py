# mypy: disable-error-code="arg-type, misc, valid-type"
import asyncio

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.order.crud import OrderCrud
from app.db.session import get_session
from app.schema.common import ManyResponse
from app.schema.order import OrderFilter, OrderOutHttp, OrderSort
from app.service.order import NewOrderService
from app.service.paginator import Paginator

router = APIRouter(prefix="/order", tags=["order"])
crud_cls = OrderCrud


@router.get(
    "",
    response_model=ManyResponse[OrderOutHttp],
)
async def get_many(
    paginator: Paginator = Depends(),
    order: OrderSort = OrderSort.DATE.desc,
    filter: OrderFilter = Depends(),
    db_session: AsyncSession = Depends(get_session),
):
    crud = crud_cls(db_session)
    items, total = await asyncio.gather(
        crud.get_many(
            paginator=paginator,
            filter_generator=filter,
            order=order.to_order_generator(),
        ),
        crud.count(filter_generator=filter),
    )
    return {
        "items": items,
        "total": total,
    }


@router.get(
    "/{id}",
    response_model=OrderOutHttp,
)
async def get_one(
    id: int,
    db_session: AsyncSession = Depends(get_session),
):
    return await crud_cls(db_session).get_by_id(id)


@router.post(
    "",
    response_model=OrderOutHttp,
)
async def create(
    data: crud_cls.schema_in,
    db_session: AsyncSession = Depends(get_session),
):
    return await NewOrderService(data, db_session).run()


@router.put(
    "/{id}",
    response_model=OrderOutHttp,
)
async def update(
    id: int,
    data: crud_cls.schema_update,
    db_session: AsyncSession = Depends(get_session),
):
    return await crud_cls(db_session).update(crud_cls.model.id == id, data)


@router.delete(
    "/{id}",
)
async def delete(
    id: int,
    db_session: AsyncSession = Depends(get_session),
):
    return await crud_cls(db_session).delete(crud_cls.model.id == id)
