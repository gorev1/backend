# mypy: disable-error-code="arg-type, misc, valid-type"

from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.session import get_session
from app.schema.user import UserOutHttp
from app.service.login import LoginData, LoginService

router = APIRouter(prefix="/login")


@router.post(
    "",
    response_model=UserOutHttp,
)
async def login(
    data: LoginData,
    db_session: AsyncSession = Depends(get_session),
):
    user = await LoginService(db_session, data).run()
    if user is None:
        return JSONResponse({"detail": "Неверный email или пароль"}, status_code=400)
    return user
