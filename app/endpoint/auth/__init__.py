# mypy: disable-error-code=attr-defined
from fastapi import APIRouter

from . import login

auth_router = APIRouter(prefix="/auth", tags=["auth"])
auth_router.include_router(login.router)
