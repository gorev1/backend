"""Модель товара."""
import sqlalchemy as sa

from app.db.base_model import BaseSqlModel


class ItemModel(BaseSqlModel):
    """Модель товара."""

    name = sa.Column(sa.String, nullable=False, index=True)
    description = sa.Column(sa.String, nullable=False)
    count = sa.Column(sa.Integer, nullable=False, default=1)

    # category_id = sa.Column(sa.Integer, sa.ForeignKey(CategoryModel.id), nullable=False)
    # category = orm.relationship(
    #     CategoryModel,
    #     cascade="save-update",
    #     lazy="joined",
    # )
    category_name = sa.Column(sa.String, nullable=False, index=True)
