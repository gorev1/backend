from app.db.base_schema import BaseActSchema, BaseOutSchema


class ItemOutSchema(BaseOutSchema):
    """Схема для вывода товара"""

    id: int
    name: str
    description: str
    category_name: str
    count: int


class ItemInSchema(BaseActSchema):
    """Схема для ввода товара"""

    name: str
    description: str = ""
    category_name: str
    count: int = 1


class ItemUpdateSchema(BaseActSchema):
    """Схема для обновления товара"""

    name: str | None
    description: str | None
    category_name: str | None
    count: int | None
