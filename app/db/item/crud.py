import sqlalchemy.orm as orm

from app.db.base_crud import BaseCrud
from app.db.category.model import CategoryModel

from .model import ItemModel
from .schema import ItemInSchema, ItemOutSchema, ItemUpdateSchema

CATEGORY = orm.aliased(CategoryModel)


class ItemCrud(BaseCrud[ItemModel, ItemOutSchema, ItemInSchema, ItemUpdateSchema]):
    ...
    # def _get_columns(self):
    #     return get_columns(ItemModel)

    # def _default_select(self):
    #     columns = self._get_columns()
    #     return sa.select(*columns, CATEGORY).select_from(ItemModel).join(CATEGORY, ItemModel.category_id == CATEGORY.id)

    # def _default_parse_to_schema(self, data: tuple | ItemModel):
    #     if isinstance(data, ItemModel):
    #         return self.schema_out(**data.__dict__, category_name=data.category.name)
    #     columns = self._get_columns()
    #     category: CategoryModel = data[-1]
    #     return self.schema_out(
    #         **{column.name: v for column, v in zip(columns, data[: len(columns)], strict=True)},
    #         category_name=category.name,
    #     )
