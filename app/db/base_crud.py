from typing import Generic, TypeGuard, TypeVar, get_args

import sqlalchemy as sa
from pydantic import BaseModel
from sqlalchemy.engine import TupleResult
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.base_model import BaseSqlModel
from app.db.base_schema import BaseActSchema, BaseOutSchema
from app.exception import NotFoundError
from app.service.filter_generator import BaseFilterGenerator
from app.service.order_generator import OrderGenerator
from app.service.paginator import Paginator

_Model = TypeVar("_Model", bound=BaseSqlModel)
_SchemaOut = TypeVar("_SchemaOut", bound=BaseOutSchema)
_SchemaIn = TypeVar("_SchemaIn", bound=BaseActSchema)
_SchemaUpdate = TypeVar("_SchemaUpdate", bound=BaseActSchema)


def is_pydantic(obj: object) -> TypeGuard[BaseActSchema]:
    """Проверяет является ли obj экземпляром модели pydantic."""
    return type(obj).__class__.__module__.startswith("pydantic.")


def create_nested_db_items(schema_instance: BaseModel | dict) -> dict:
    """
    Итерируется по экзэмпляру pydantic модели и трансформирует вложенные
    модели в SQLAlchemy модели.
    Функция работает, если во вложенных pydantic моделях прописан
    Meta.orm_model.
    """
    model_attributes: dict = dict(schema_instance)
    for key, value in model_attributes.items():
        if is_pydantic(value):  # pylint: disable=R5601
            try:
                model_attributes[key] = value.Config.orm_model(**create_nested_db_items(value.dict()))
            except AttributeError as exc:
                raise AttributeError(
                    f"Найдена вложенная Pydantic({(type(value))}) модель, но атрибут " "Config.orm_model не определен."
                ) from exc
        elif isinstance(value, list):
            model_attributes[key] = [create_nested_db_items(schema) for schema in value]
    return model_attributes


class BaseCrud(Generic[_Model, _SchemaOut, _SchemaIn, _SchemaUpdate]):
    model: type[_Model]
    schema_out: type[_SchemaOut]
    schema_in: type[_SchemaIn]
    schema_update: type[_SchemaUpdate]

    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session

    def __init_subclass__(cls, *args, **kwargs):
        super().__init_subclass__(*args, **kwargs)
        if (
            generic_info := next(
                (base for base in cls.__dict__.get("__orig_bases__", []) if base.__origin__ is BaseCrud),
                None,
            )
        ) is None:
            return
        model, schema_out, schema_in, schema_update = get_args(generic_info)
        cls.model = model
        cls.schema_out = schema_out
        cls.schema_in = schema_in
        cls.schema_update = schema_update

    def _default_select(self) -> sa.Select:
        """Возвращает select по умолчанию"""
        return sa.select(self.model)

    def _default_parse_to_schema(self, res: _Model | TupleResult) -> _SchemaOut:
        """Преобразует результат запроса к схеме"""
        if isinstance(res, sa.Row) and len(res) == 1:
            res = res[0]
        if isinstance(res, self.model):
            return self.schema_out.from_orm(res)
        raise NotImplementedError()

    def _parse_result(self, result: sa.engine.Result) -> _SchemaOut | None:
        """Преобразует результат запроса к схеме"""
        if ans := result.tuples().one_or_none():
            return self._default_parse_to_schema(ans)
        return None

    def _parse_row(self, row: sa.engine.Row) -> _SchemaOut:
        """Преобразует результат запроса к схеме"""
        tup = row.tuple()
        if len(tup) == 1:
            return self._default_parse_to_schema(tup[0])
        return self._default_parse_to_schema(tup)

    async def create(self, schema: _SchemaIn) -> _SchemaOut:
        """Создает объект"""
        model = self.model(**create_nested_db_items(schema.dict()))
        self.db_session.add(model)
        await self.db_session.commit()
        await self.db_session.flush()
        return await self.get(model.id == self.model.id)  # type: ignore[return-value]

    async def count(
        self,
        select_statement: sa.Select | None = None,
        filter_generator: BaseFilterGenerator | None = None,
    ) -> int:
        """Считает количество объектов по переданным фильтрам"""
        select_st = select_statement or self._default_select()

        if filter_generator:
            select_st = filter_generator.update_select(select_st)

        res: sa.Result = await self.db_session.execute(sa.select(sa.func.count()).select_from(select_st))  # type: ignore[arg-type]
        return res.scalar_one()

    async def get_by_id(self, id: int) -> _SchemaOut | None:
        """Возвращает объект по id"""
        res: sa.Result = await self.db_session.execute(self._default_select().where(self.model.id == id))
        if isinstance(res, sa.engine.ChunkedIteratorResult):
            item = res.first()
            if not item:
                raise NotFoundError
            return self._parse_row(item)
        return self._parse_result(res)

    async def get(self, where_expression: sa.BinaryExpression | sa.ColumnElement[bool]) -> _SchemaOut | None:
        """Возвращает объект по переданным фильтрам"""
        res = await self.db_session.execute(self._default_select().where(where_expression))
        return self._parse_result(res)

    async def get_many(
        self,
        filter_generator: BaseFilterGenerator | None = None,
        paginator: Paginator | None = None,
        order: OrderGenerator | None = None,
    ) -> list[_SchemaOut]:
        """Возвращает все объекты по переданным фильтрам"""
        select_st = self._default_select()

        for select_updatable in (paginator, order, filter_generator):
            if select_updatable is not None:
                select_st = select_updatable.update_select(select_st)

        res: sa.Result = await self.db_session.execute(select_st)
        return [self._parse_row(row) for row in res.unique().all()]

    async def update(self, where_expression: sa.BinaryExpression, schema: _SchemaUpdate) -> _SchemaOut:
        """Обновляет объект по переданным фильтрам"""
        await self.db_session.execute(
            sa.update(self.model).where(where_expression).values(**schema.dict(exclude_unset=True))
        )
        await self.db_session.commit()
        ans = await self.get(where_expression)
        assert ans is not None
        return ans

    async def delete(self, where_expression: sa.BinaryExpression) -> None:
        """Удаляет объект по переданным фильтрам"""
        await self.db_session.execute(sa.delete(self.model).where(where_expression))
        await self.db_session.commit()
