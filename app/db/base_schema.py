"""Базовые схемы"""
from pydantic import BaseModel


class BaseActSchema(BaseModel):
    """Схемы для работы с базой данных"""

    class Config:
        """Конфиги для pydantic"""

        orm_model: type[BaseModel]
        allow_population_by_field_name = True


class BaseOutSchema(BaseModel):
    """Схемы для работы с базой данных"""

    class Config:
        """Конфиги для pydantic"""

        orm_mode = True
