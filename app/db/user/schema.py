from enum import StrEnum

from app.db.base_schema import BaseActSchema, BaseOutSchema
from app.utils.pydantic_validators import DecodePhone, EncodePhone, HashPassword


class UserRole(StrEnum):
    ADMIN = "admin"
    USER = "user"


class UserOutSchema(BaseOutSchema):
    """Схема для вывода пользователя"""

    id: int
    fio: str
    email: str
    phone_number: DecodePhone
    hashed_password: str
    status: str
    position: str
    role: UserRole

    def validate_password(self, password: str):
        return HashPassword.hash(password) == self.hashed_password


class UserInSchema(BaseActSchema):
    """Схема для ввода пользователя"""

    fio: str
    email: str
    hashed_password: HashPassword
    phone_number: EncodePhone
    status: str
    position: str
    role: UserRole


class UserUpdateSchema(BaseActSchema):
    """Схема для обновления пользователя"""

    fio: str | None
    email: str | None
    hashed_password: HashPassword | None
    phone_number: EncodePhone | None
    status: str | None
    position: str | None
    role: UserRole | None
