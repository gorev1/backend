"""Модель пользователя."""
import sqlalchemy as sa

from app.db.base_model import BaseSqlModel


class UserModel(BaseSqlModel):
    """Модель пользователя."""

    fio = sa.Column(sa.String, nullable=False)
    email = sa.Column(sa.String, nullable=False, unique=True, index=True)
    hashed_password = sa.Column(sa.String(length=64), nullable=False)
    phone_number = sa.Column(sa.String, nullable=False)
    status = sa.Column(sa.String, nullable=False)
    position = sa.Column(sa.String, nullable=False)
    role = sa.Column(sa.String, nullable=False)
