from app.db.base_crud import BaseCrud

from .model import UserModel
from .schema import UserInSchema, UserOutSchema, UserUpdateSchema


class UserCrud(BaseCrud[UserModel, UserOutSchema, UserInSchema, UserUpdateSchema]):
    ...
