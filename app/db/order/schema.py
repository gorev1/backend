from datetime import datetime
from enum import StrEnum

from app.db.base_schema import BaseActSchema, BaseOutSchema
from app.db.item.schema import ItemOutSchema
from app.db.user.schema import UserOutSchema


class OrderType(StrEnum):
    """Статус заказа"""

    TAKEN = "Взять"
    RETURNED = "Вернуть"


class OrderOutSchema(BaseOutSchema):
    """Схема для вывода заказа"""

    item: ItemOutSchema
    user: UserOutSchema
    date: datetime | None
    how_much: float
    type: OrderType


class OrderInSchema(BaseActSchema):
    """Схема для ввода заказа"""

    item_id: int
    user_id: int
    date: datetime
    how_much: float
    type: OrderType


class OrderUpdateSchema(BaseActSchema):
    """Схема для обновления заказа"""

    item_id: int | None
    user_id: int | None
    date: datetime | None
    how_much: float
    type: OrderType | None
