import sqlalchemy as sa
import sqlalchemy.orm as orm

from app.db.base_crud import BaseCrud
from app.db.item.model import ItemModel
from app.db.item.schema import ItemOutSchema
from app.db.user.model import UserModel

from .model import OrderModel
from .schema import OrderInSchema, OrderOutSchema, OrderUpdateSchema

USER = orm.aliased(UserModel)
ITEM = orm.aliased(ItemModel)
# CATEGORY = orm.aliased(CategoryModel)


class OrderCrud(BaseCrud[OrderModel, OrderOutSchema, OrderInSchema, OrderUpdateSchema]):
    def _get_columns(self):
        return [v for k, v in OrderModel.__table__.c.items() if k not in {"id"}]

    def _default_select(self):
        columns = self._get_columns()
        return (
            sa.select(*columns, USER, ITEM)
            .select_from(OrderModel)
            .join(USER, OrderModel.user_id == USER.id)
            .join(ITEM, OrderModel.item_id == ITEM.id)
            # .join(CATEGORY, ITEM.category_id == ITEM.id)
        )

    def _default_parse_to_schema(self, data: tuple):  # type: ignore[override]
        columns = self._get_columns()
        item_sql = data[-1]
        return self.schema_out(
            **{column.name: v for column, v in zip(columns, data[: len(columns)], strict=True)},
            user=data[-2],
            item=ItemOutSchema(
                **item_sql.__dict__,
                # category_name=item_sql.category.name,
            ),
        )
