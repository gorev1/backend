import sqlalchemy as sa

from app.db.base_model import BaseSqlModel


class OrderModel(BaseSqlModel):
    """Модель заказа."""

    user_id = sa.Column(sa.BigInteger, sa.ForeignKey("user.id"), nullable=False, index=True)
    item_id = sa.Column(sa.BigInteger, sa.ForeignKey("item.id"), nullable=False, index=True)
    how_much = sa.Column(sa.Float, nullable=False)
    date = sa.Column(sa.DateTime(timezone=True), nullable=False, default=sa.func.now())
    type = sa.Column(sa.String, nullable=False)
