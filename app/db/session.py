"""Инициализация подключения к базе данных."""
from collections.abc import AsyncIterator
from typing import Any, Callable

from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from app.config import Config

from .base_model import BaseSqlModel

settings = Config.database


class AsyncDBEngine:
    """Класс для работы с асинхронным движком БД."""

    def __init__(self, db_settings: dict[str, Any]):
        self.engine: AsyncEngine = create_async_engine(**db_settings)

    async def create_tables(self):
        """Проверяет наличие таблиц в БД и создаёт отсутствующие."""
        # pylint: disable=locally-disabled, no-member
        async with self.engine.begin() as conn:
            await conn.run_sync(BaseSqlModel.metadata.create_all)

    async def close_connections(self):
        """Закрывает активные соединения с БД."""
        await self.engine.dispose()


async_engine = AsyncDBEngine(settings.config())


# expire_on_commit=False will prevent attributes from being expired
# after commit.
async_session: Callable[[], AsyncSession] = sessionmaker(
    async_engine.engine,  # type: ignore[call-overload]
    expire_on_commit=False,
    class_=AsyncSession,
    future=True,
    autocommit=False,
    autoflush=False,
)


async def get_session() -> AsyncIterator[AsyncSession]:
    """
    Генерирует новый объект сессии базы данных для работы внутри одного endpoint.

    По завершении работы с ним - закрывает.
    """
    async with async_session() as session:
        yield session
