"""Модель товара."""
import sqlalchemy as sa
from sqlalchemy_utils import LtreeType

from app.db.base_model import BaseSqlModel


class CategoryModel(BaseSqlModel):
    name = sa.Column(sa.String, nullable=False, index=True, unique=True)
    path: sa.Column[str] = sa.Column(LtreeType, nullable=False, index=True, unique=True)
