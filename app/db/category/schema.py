from app.db.base_schema import BaseActSchema, BaseOutSchema
from app.utils.pydantic_validators import JustLtree, ToLtree


class CategoryOutSchema(BaseOutSchema):
    """Схема для вывода товара"""

    id: int
    name: str
    path: JustLtree


class CategoryInSchema(BaseActSchema):
    """Схема для ввода товара"""

    name: str
    path: ToLtree


class CategoryUpdateSchema(BaseActSchema):
    """Схема для обновления товара"""

    name: str | None
    path: ToLtree | None
