from app.db.base_crud import BaseCrud

from .model import CategoryModel
from .schema import CategoryInSchema, CategoryOutSchema, CategoryUpdateSchema


class CategoryCrud(BaseCrud[CategoryModel, CategoryOutSchema, CategoryInSchema, CategoryUpdateSchema]):
    ...
