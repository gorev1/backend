from collections.abc import Callable
from dataclasses import dataclass
from functools import wraps
from typing import Annotated, Any, ClassVar, get_origin

import sqlalchemy as sa
from pydantic import BaseModel

from app.db.base_model import BaseSqlModel
from app.service.abc import SelectUpdatable


@dataclass
class FilterRepresentation:
    """Обертка об информации фильтра"""

    func: Callable[[Any], sa.BinaryExpression]
    value: Any


# pylint: disable=invalid-name
class Rule:
    """Базовые фильтры"""

    @staticmethod
    def like(model: BaseSqlModel, field: str):
        """Реализация sql функции like"""
        column: sa.Column = getattr(model, field)

        def wrapped(value: str):
            return sa.func.lower(column).like(f"%{value.lower()}%")

        return wrapped

    @staticmethod
    def eq(model: BaseSqlModel, field: str):
        """Реализация sql функции eq"""
        column: sa.Column = getattr(model, field)

        def wrapped(value: str):
            return column == value

        return wrapped

    @staticmethod
    def gt(model: BaseSqlModel, field: str):
        """Реализация sql функции gt"""
        column: sa.Column = getattr(model, field)

        def wrapped(value: str):
            return column > value

        return wrapped

    @staticmethod
    def ge(model: BaseSqlModel, field: str):
        """Реализация sql функции ge"""
        column: sa.Column = getattr(model, field)

        def wrapped(value: str):
            return column >= value

        return wrapped

    @staticmethod
    def le(model: BaseSqlModel, field: str):
        """Реализация sql функции le"""
        column: sa.Column = getattr(model, field)

        def wrapped(value: str):
            return column <= value

        return wrapped

    @staticmethod
    def lt(model: BaseSqlModel, field: str):
        """Реализация sql функции lt"""
        column: sa.Column = getattr(model, field)

        def wrapped(value: str):
            return column < value

        return wrapped


class _Meta(type(BaseModel)):  # type: ignore[misc]
    """Метакласс для генерации фильтров"""

    # pylint: disable=bad-mcs-classmethod-argument
    def __new__(meta_cls, name, bases, namespace, **kwargs):
        cls: BaseFilterGenerator = super().__new__(meta_cls, name, bases, namespace, **kwargs)
        for field_name, field_info in cls.__fields__.items():
            if field_name.startswith("_"):
                continue

            if (annotation := namespace.get("__annotations__", {}).get(field_name, None)) is None:
                continue

            if get_origin(annotation) is Annotated:
                func: Callable[[Any], sa.BinaryExpression] = annotation.__metadata__[0]
            else:
                try:
                    field, rule_name = field_name.split("__", 1)
                except ValueError as exc:
                    raise ValueError(f"Field {field_name} must be annotated or have __rule_name suffix") from exc
                rule: Callable[[Any, str], Callable[[Any], sa.BinaryExpression]] = getattr(Rule, rule_name)
                func = rule(cls._MODEL, field)
            validator = meta_cls.wrap_to_validator(func)
            field_info.validators.append(validator)
        return cls

    @staticmethod
    def wrap_to_validator(func: Callable):
        """Оборачиваем функцию в валидатор, чтобы добавить его в pydantic"""

        # pylint: disable=unused-argument
        @wraps(func)
        def wrapped(cls: type[BaseModel], value: Any, *_, **__):
            if value is None:
                return None
            return FilterRepresentation(func, value)

        return wrapped


class BaseFilterGenerator(BaseModel, SelectUpdatable, metaclass=_Meta):  # type: ignore[misc]
    """
    Базовый класс для генерации фильтров.
    Работает так:
        если поле имеет аннотацию typing.Annotated, то берется 2ой аргумент аннотации для создания
        фильтра. Этот аргумент должен быть функцией, которая принимает 1 аргумент - значение поля.
        пример:
            name: Annotated[str, lambda x: Table.name.like(f"%{x}%")]

        если поле не имеет аннотации, то мы мы пытаемся подобрать дефолтный фильтр по имени поля.
        Поле должно иметь такой формат: <имя колонки>__<имя фильтра>
        пример:
            name__like: будет создан фильтр, который в таблице будет искать колонку name и
            применит функцию Rule.like к ней
    """

    _MODEL: ClassVar[type[BaseSqlModel]]

    def get_where_expression(self, *additional: sa.BinaryExpression) -> sa.BinaryExpression:
        """
        Возвращаем where для фильтрации в sqlalchemy
        """
        return sa.and_(
            True,
            *[
                answer
                for item in self.dict(exclude_unset=True).values()
                if (isinstance(item, FilterRepresentation) and (answer := item.func(item.value)) is not None)
            ],
            *additional,
        )  # type: ignore[return-value]

    def update_select(self, select: sa.Select) -> sa.Select:
        return select.where(self.get_where_expression())
