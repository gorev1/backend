from abc import ABC, abstractmethod

import sqlalchemy as sa


class SelectUpdatable(ABC):
    @abstractmethod
    def update_select(self, select: sa.Select) -> sa.Select:
        raise NotImplementedError()
