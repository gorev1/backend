import sqlalchemy as sa
from pydantic import BaseModel

from app.service.abc import SelectUpdatable


class Paginator(BaseModel, SelectUpdatable):
    page: int = 1
    limit: int = 10

    def update_select(self, select: sa.Select) -> sa.Select:
        return select.offset((self.page - 1) * self.limit).limit(self.limit)
