from dataclasses import dataclass

from pydantic import BaseModel
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.user.crud import UserCrud


class LoginData(BaseModel):
    email: str
    password: str


@dataclass
class LoginService:
    session: AsyncSession
    data: LoginData

    def __post_init__(self):
        self.user_crud = UserCrud(self.session)

    async def run(self):
        user = await self.user_crud.get(self.user_crud.model.email == self.data.email)
        if user is None:
            return None

        if not user.validate_password(self.data.password):
            return None
        return user
