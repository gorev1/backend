from dataclasses import dataclass

from sqlalchemy.ext.asyncio import AsyncSession

from app.db.item.crud import ItemCrud
from app.db.order.crud import OrderCrud
from app.db.order.schema import OrderInSchema, OrderType
from app.exception import NotFoundError, ValidationError


@dataclass
class NewOrderService:
    order: OrderInSchema
    session: AsyncSession

    def __post_init__(self):
        self.order_crud = OrderCrud(self.session)
        self.item_crud = ItemCrud(self.session)

    async def run(self):
        item = await self.item_crud.get_by_id(self.order.item_id)
        if item is None:
            raise NotFoundError
        if self.order.type is OrderType.TAKEN and item.count < self.order.how_much:
            raise ValidationError("Недостаточно товара")
        k = -1 if self.order.type is OrderType.TAKEN else 1
        update_item = self.item_crud.schema_update(count=item.count + k * self.order.how_much)
        order = await self.order_crud.create(self.order)
        await self.item_crud.update(self.item_crud.model.id == self.order.item_id, update_item)
        return order
