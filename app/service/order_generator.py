"""Вспомогательный функционал для создания order_by"""
from typing import Any, ClassVar

import pydantic.json
import sqlalchemy as sa
from pydantic import BaseModel
from sqlalchemy import Column, UnaryExpression, asc, desc

from app.service.abc import SelectUpdatable


class _OrderRepresentation(UnaryExpression):  # pylint: disable=too-many-ancestors, abstract-method
    """
    Выполняет несколько функций:
    1. человекочитаемое отображение order_expression для дебагинга
    2. является default значением, чтобы его можно было использовать как в sqlalchemy,
       так и в fastapi(swagger)
    3. перевод в OrderGenerator
    """

    _human_label: str

    @classmethod
    def new(cls, human_label, order: UnaryExpression) -> UnaryExpression:
        """Создает новый UnaryExpression который легко дебажить"""
        new_order = UnaryExpression.__new__(cls)
        new_order._human_label = human_label  # pylint: disable=protected-access
        new_order.__dict__.update(order.__dict__)
        return new_order

    def __str__(self):
        return self._human_label

    def __repr__(self):
        return f"Order(human={self._human_label!r}, sql={UnaryExpression.__str__(self)!r})"

    def to_order_generator(self) -> "OrderGenerator":
        """Перевод в OrderGenerator"""

        class _OrderGenerator(OrderGenerator):
            value = self

        return _OrderGenerator()

    @staticmethod
    def __get_validators__():
        yield lambda x: x

    # fastapi для дефолтных значений делает deepcopy, а sqlalchemy со временем может поменять
    # свой UnaryExpression, что для него нельзя будет использовать в deepcopy,
    # так что переписываем deepcopy.
    def __deepcopy__(self, memo: dict):
        return self.__copy__()

    def __copy__(self):
        return self.new(self._human_label, self)


class OrderItem:
    """Элемент для OrderEnum"""

    def __init__(self, field: str | Column, human_name: str | None = None) -> None:
        self.field = field  # используется на беке
        self.human_label = human_name or (field if isinstance(field, str) else field.name)

    def __call__(self, human_label: str):
        func = desc if human_label.startswith("-") else asc
        return func(self.field)

    @property
    def asc(self):
        """
        Возвращает сортировку от меньшего к большему.
        Нужно для указания дефолтного значения.
        Возвращает представление для fastapi(swagger) и объект который можно использовать в sqlalchemy.order_by
        """
        return _OrderRepresentation.new(f"{self.human_label}", asc(self.field))

    @property
    def desc(self):
        """
        Возвращает сортировку от большего к меньшему.
        Нужно для указания дефолтного значения.
        Возвращает представление для fastapi(swagger) и объект который можно использовать в sqlalchemy.order_by
        """
        return _OrderRepresentation.new(f"-{self.human_label}", desc(self.field))

    def __str__(self):
        return self.human_label

    def __repr__(self):
        return f"{type(self).__name__}[{self.field}, {self.human_label}]"


# pylint: disable=no-member
pydantic.json.ENCODERS_BY_TYPE[OrderItem] = str
pydantic.json.ENCODERS_BY_TYPE[_OrderRepresentation] = str


class OrderEnumMeta(type):
    """Meta для OrderEnum"""

    _members_map_: dict[str, OrderItem]

    # pylint: disable=unused-argument
    def __init__(cls, name: str, bases: tuple[type, ...], init_dict: dict):
        cls._members_map_ = dict(cls._full_members_generator(init_dict))

    @staticmethod
    def _full_members_generator(init_dict: dict):
        for item in init_dict.values():
            if not isinstance(item, OrderItem):
                continue
            yield item.human_label, item
            yield f"-{item.human_label}", item


class OrderEnum(metaclass=OrderEnumMeta):
    """
    Возможные элементы для сортировки.
    Используется почти также как обычный Enum, только для каждого элемента надо
    явно указать value как OrderItem
    """

    SWAGGER_TYPE: ClassVar[str] = "string"

    @classmethod
    def __modify_schema__(cls, field_schema: dict) -> None:
        field_schema["type"] = cls.SWAGGER_TYPE
        field_schema["enum"] = tuple(cls._members_map_.keys())

    @classmethod
    def __get_validators__(cls):
        yield cls.get_sql_order

    @classmethod
    def get_sql_order(cls, human_label: Any):
        """По label получаем order_by"""
        if isinstance(human_label, str):
            try:
                order = cls._members_map_[human_label](human_label)
                return _OrderRepresentation.new(human_label, order)
            except KeyError as err:
                raise ValueError(
                    "value is not a valid enumeration member; permitted: "
                    + ", ".join(f"{v!r}" for v in cls._members_map_.keys())
                ) from err
        return human_label

    def to_order_generator(self) -> "OrderGenerator":
        """
        Заглушка для линтеров. На самом деле будет дергаться метод _OrderRepresentation.to_order_generator
        Нужно когда order в методе один, и не удобно использовать OrderGenerator
        """
        raise NotImplementedError()


class OrderGenerator(BaseModel, SelectUpdatable):
    def update_select(self, select: sa.Select) -> sa.Select:
        """Обновляет select согласно order_by"""
        orders = self.dict(exclude_none=True).values()
        return select.order_by(*orders)
