from dataclasses import dataclass

from fastapi import HTTPException


@dataclass
class ServerError(Exception):
    """Базовая ошибка"""

    message: str = "server_error"
    detail: str = "Что то пошло не так"
    status_code: int = 500
    http_error: HTTPException = None  # type: ignore[assignment]
    level: str = "error"

    def __post_init__(self):
        if not self.http_error:
            self.http_error = HTTPException(self.status_code, self.message)

    def __init_subclass__(cls) -> None:
        super().__init_subclass__()
        if cls.__init__ is ServerError.__init__:
            cls.__init__.__defaults__ = (cls.message, cls.detail, cls.http_error, cls.level)


class NotFoundError(ServerError):
    """Класс исключения для ошибок 404"""

    message = "not_found"
    detail = "Not Found"


class ValidationError(ServerError):
    """Класс исключения для ошибок 422"""

    message = "validation_error"
    detail = "Ошибка валидации"
    status_code = 422
