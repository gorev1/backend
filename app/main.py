# mypy: disable-error-code=attr-defined
"""Запуск приложения"""
from collections.abc import Mapping
from logging import getLogger

import sentry_sdk
import uvicorn
from fastapi import FastAPI, Request, status
from fastapi.responses import JSONResponse
from pydantic import ValidationError
from starlette.exceptions import HTTPException as StarletteHTTPException
from starlette.middleware.cors import CORSMiddleware

from app.config import Config
from app.db.session import async_engine
from app.endpoint.auth import auth_router
from app.endpoint.http_crud import crud_router

settings = Config
logger = getLogger()


app = FastAPI(
    title="store",
    root_path=settings.root_path,
    openapi_url="/api/openapi.json",
    version="1",
    description="Что-то про склад",
)
app.include_router(crud_router)
app.include_router(auth_router)
app.add_middleware(
    CORSMiddleware,
    allow_credentials=True,
    allow_headers=["*"],
    allow_origins=["*"],
    allow_methods=["*"],
)


# @app.middleware("http")
# async def logging_middleware(
#     request: Request,
#     call_next: Callable[[Request], Coroutine[None, None, Response]],
# ):
#     """Делаем всякие штуки для логирования"""
#     context_from_request = {
#         key[len(settings.context_headers_prefix) :].lower(): value
#         for key, value in request.headers.items()
#         if key.lower().startswith(settings.context_headers_prefix)
#     }
#     trace_id = context_from_request.pop("trace_id", str(uuid4()))
#     CONTEXT.update(
#         **{
#             "trace_id": trace_id,
#             "method": f"{request.method} {request.url.path}",
#             **context_from_request,
#         }
#     )
#     sentry_sdk.set_tag("trace_id", trace_id)
#     with metrics_contextmanager():
#         return await call_next(request)


@app.middleware("http")
async def allow_https_proxy(request: Request, call_next):
    """добавляем поддержку опции https_proxy_enabled"""
    if "/worker-api/" in request.url.path:
        pass
    elif settings.https_proxy_enabled and request.scope["scheme"] == "http":
        request.scope["scheme"] = "https"

    response = await call_next(request)
    logger.debug(f"out {request.url=}")

    return response


@app.exception_handler(Exception)
async def log_error(_: Request, exception: Exception):
    """Логирование не обработанных ошибок"""
    sentry_sdk.capture_exception(exception)
    sentry_id = sentry_sdk.last_event_id()
    logger.exception(exception, extra={"sentry_id": sentry_id})
    return JSONResponse(
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        content={
            "message": "Возникла ошибка",
            "detail": f"sentry_id: {sentry_id}",
            "level": "error",
        },
    )


# @app.exception_handler(ServerError)
# async def convert_to_http_error(request: Request, exception: ServerError):
#     """Обработка для ServerError"""
#     return await http_exception_handler(request, exception.http_error)


@app.exception_handler(StarletteHTTPException)
async def http_exception_handler(_, exception: StarletteHTTPException):
    """Http exception handler"""
    logger.exception(exception)
    return JSONResponse(
        status_code=exception.status_code,
        content={
            "message": "Возникла ошибка",
            "detail": exception.detail,
            "level": "error",
        },
    )


def get_field_name(error_dict: Mapping) -> str | None:
    """Получает путь к полю или просто его названия"""
    try:
        return ".".join(map(str, error_dict["loc"]))
    except (KeyError, TypeError):
        return None


@app.exception_handler(ValidationError)
async def validation_error_exception_handler(_: Request, exception: ValidationError):
    """Глобально обрабатывает исключение ValidationError, поднимаемое
    pydantic в процессе валидации.

    Используется для отправки ответа в установленном формате.
    Поскольку ответ на запрос пользователя содержит минимум технической
    информации используется логгер для фиксации ошибок.
    """
    errors = exception.errors()
    logger.exception(exception)
    error_details = {get_field_name(error) or number: error.get("msg") for number, error in enumerate(errors, start=1)}
    return JSONResponse(
        status_code=status.HTTP_400_BAD_REQUEST,
        content={
            "message": "Невозможно выполнить операцию.",
            "detail": error_details,
            "level": "error",
        },
    )


@app.on_event("startup")
async def startup_event():
    """Handle startup"""
    # sentry_init()


@app.on_event("shutdown")
async def shutdown():
    """Закрывает интерфейсы приложения"""
    await async_engine.close_connections()


if __name__ == "__main__":
    uvicorn.run(
        app,
        host=settings.http_service.host,
        port=settings.http_service.port,
        # log_config=settings.logging.config(),
        # log_level=settings.logging.log_level.lower(),
    )
