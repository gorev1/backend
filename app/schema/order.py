from typing import Annotated

from app.db.order.model import OrderModel
from app.db.order.schema import OrderOutSchema
from app.schema.user import UserOutHttp
from app.service.filter_generator import BaseFilterGenerator
from app.service.order_generator import OrderEnum, OrderItem


class OrderSort(OrderEnum):
    DATE = OrderItem(OrderModel.date, "date")


class OrderFilter(BaseFilterGenerator):  # type: ignore[misc]
    _MODEL = OrderModel

    item_id: Annotated[int | None, lambda x: OrderModel.item_id == x]
    user_id: Annotated[int | None, lambda x: OrderModel.user_id == x]


class OrderOutHttp(OrderOutSchema):
    """Схема для вывода http"""

    user: UserOutHttp  # type: ignore[assignment]
