from pydantic import BaseModel

from app.db.user.schema import UserRole


class UserOutHttp(BaseModel):
    id: int
    fio: str
    email: str
    phone_number: str
    status: str
    position: str
    role: UserRole


class UserInHttp(BaseModel):
    fio: str
    email: str
    password: str
    phone_number: str
    status: str
    position: str
    role: UserRole


class UserUpdateHttp(BaseModel):
    fio: str | None
    email: str | None
    password: str | None
    phone_number: str | None
    status: str | None
    position: str | None
    role: UserRole | None
