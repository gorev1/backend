from app.db.category.model import CategoryModel
from app.service.order_generator import OrderEnum, OrderItem


class CategoryOrder(OrderEnum):
    NAME = OrderItem(CategoryModel.name, "name")
    PATH = OrderItem(CategoryModel.path, "path")
