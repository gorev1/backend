from typing import Generic, TypeVar

from pydantic.generics import GenericModel

_T = TypeVar("_T")


class ManyResponse(GenericModel, Generic[_T]):
    items: list[_T]
    total: int
