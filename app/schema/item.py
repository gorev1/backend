from typing import Annotated

from app.db.item.model import ItemModel
from app.service.filter_generator import BaseFilterGenerator
from app.service.order_generator import OrderEnum, OrderItem


class ItemOrder(OrderEnum):
    ID = OrderItem(ItemModel.id, "id")
    NAME = OrderItem(ItemModel.name, "name")
    CATEGORY = OrderItem(ItemModel.category_name, "category")
    COUNT = OrderItem(ItemModel.count, "count")


class ItemFilter(BaseFilterGenerator):  # type: ignore[misc]
    _MODEL = ItemModel

    name__like: str | None
    category__like: Annotated[str | None, lambda x: ItemModel.category_name.like(f"{x}.%")]
    count__ge: int | None
    count__le: int | None
