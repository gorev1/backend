import hashlib

import pydantic.json
from sqlalchemy_utils import Ltree

from app.utils.cipher import PhoneCipher


class HashPassword(str):
    """Класс для хеширования пароля"""

    @classmethod
    def __get_validators__(cls):
        yield cls.hash

    @classmethod
    def hash(cls, v: str) -> str:
        return hashlib.sha256(v.encode()).hexdigest()


class EncodePhone(str):
    """Класс для шифрования телефонного номера"""

    @classmethod
    def __get_validators__(cls):
        yield cls._encode

    @classmethod
    def _encode(cls, v: str) -> str:
        return PhoneCipher(v).encode()


class DecodePhone(str):
    """Класс для дешифрования телефонного номера"""

    @classmethod
    def __get_validators__(cls):
        yield cls._decode

    @classmethod
    def _decode(cls, v: str) -> str:
        return PhoneCipher(v).decode()


class ToLtree(str):
    """Класс для преобразования строки в Ltree"""

    @classmethod
    def __get_validators__(cls):
        yield cls._to_ltree

    @classmethod
    def _to_ltree(cls, v: str) -> Ltree:
        return Ltree(v)


class FromLtree(str):
    """Класс для преобразования Ltree в строку"""

    @classmethod
    def __get_validators__(cls):
        yield cls._from_ltree

    @classmethod
    def _from_ltree(cls, v: Ltree) -> str:
        return str(v)


class JustLtree(Ltree):
    """Класс для преобразования Ltree в строку"""

    @classmethod
    def __get_validators__(cls):
        yield lambda x: x

    @staticmethod
    def __modify_schema__(field_schema: dict):
        field_schema.update(type="string")


# pylint: disable=no-member
pydantic.json.ENCODERS_BY_TYPE[Ltree] = str
