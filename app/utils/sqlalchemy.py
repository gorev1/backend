from sqlalchemy import Column

from app.db.base_model import BaseSqlModel


def get_columns(
    model: type[BaseSqlModel],
    *,
    include: set[str] = set(),  # noqa: B006
    exclude: set[str] = set(),  # noqa: B006
) -> list[Column]:
    """
    Возвращает список колонок модели.
    include - список колонок, которые нужно включить в результат.
    exclude - список колонок, которые нужно исключить из результата.
    нельзя одновременно использовать include и exclude.
    """
    assert not (include and exclude), "Нельзя одновременно использовать include и exclude."
    if include:
        check = lambda k: k in include
    elif exclude:
        check = lambda k: k not in exclude
    else:
        check = lambda k: True
    return [v for k, v in model.__table__.c.items() if check(k)]
