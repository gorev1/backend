import os
from dataclasses import dataclass
from typing import ClassVar

from cryptography.hazmat.primitives import padding
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

from app.config.config import Config


@dataclass
class PhoneCipher:
    phone: str

    SECRET_KEY: ClassVar[bytes] = bytes.fromhex(Config.cipher.secret)

    def _get_cipher(self, iv: bytes | None = None) -> Cipher:
        if not iv:
            iv = os.urandom(16)
        return Cipher(algorithms.AES(self.SECRET_KEY), modes.CBC(iv))

    def _get_padder(self):
        return padding.PKCS7(128)

    def encode(self) -> str:
        """Шифрует номер телефона"""
        iv = os.urandom(16)
        cipher = self._get_cipher(iv).encryptor()
        padder = self._get_padder().padder()

        value = padder.update(self.phone.encode()) + padder.finalize()
        encoded_phone: bytes = cipher.update(value) + cipher.finalize()
        return f"{encoded_phone.hex()}:{iv.hex()}"

    def decode(self) -> str:
        """Дешифрует номер телефона"""
        padder = self._get_padder().unpadder()
        encoded_phone, iv = map(bytes.fromhex, self.phone.split(":"))
        cipher = self._get_cipher(iv).decryptor()
        padded_value = cipher.update(encoded_phone)
        phone = padder.update(padded_value) + padder.finalize()
        return phone.decode("utf-8")
