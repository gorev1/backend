"""add admin user

Revision ID: 011354ed170f
Revises: 03a5c1d333fe
Create Date: 2023-05-08 11:47:57.582317

"""
from alembic import op
import sqlalchemy as sa

from app.db.user.model import UserModel
from app.db.user.schema import UserInSchema, UserRole


# revision identifiers, used by Alembic.
revision = '011354ed170f'
down_revision = '03a5c1d333fe'
branch_labels = None
depends_on = None


admin_email = "admin"

def upgrade() -> None:
    connection = op.get_bind()
    user = UserInSchema(
        fio="admin",
        email=admin_email,
        hashed_password="admin",
        phone_number="79180000000",
        status="active",
        position="admin",
        role=UserRole.ADMIN,
    )
    connection.execute(sa.insert(UserModel).values(**user.dict()))


def downgrade() -> None:
    connection = op.get_bind()
    connection.execute(
        sa.delete(UserModel)
        .where(UserModel.email == admin_email)
    )
